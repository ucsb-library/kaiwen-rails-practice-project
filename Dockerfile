ARG RUBY_VERSION=3.0.0
FROM ruby:$RUBY_VERSION-alpine 

RUN apk --no-cache upgrade && \
  apk add --no-cache \
  build-base \
  git \
  postgresql-client \
  postgresql-dev \
  tzdata

RUN gem update bundler

WORKDIR /app

COPY . /app/
RUN ls -al 
RUN bundle check || bundle install --jobs "$(nproc)"
RUN POSTGRES_HOST='fake' bundle exec rake assets:precompile

# CMD ["bundle", "exec", "rails", "s"]
CMD ["bundle", "exec", "puma", "-b", "tcp://0.0.0.0:3000"]